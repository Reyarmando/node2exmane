const mongoose = require('mongoose');
const { Schema } = mongoose;

const NewSchema = new Schema({
    title: { type: String },
    img: { type: String },
    description: { type:String },
    date: { type:Date, default: Date.now },
    coment: { type:String }
})



module.exports = mongoose.model('New', NewSchema)
