const router = require('express').Router();

const New = require('../models/New');


router.get('/news/add', (req, res) => {
    res.render('news/new-news')
});

router.post('/news/new-news', async (req, res) => {
    const { title, description, img } = req.body;
    const errors = [];
    if (!title) {
      errors.push({ text: "Please Write a Title." });
    }
    if (!description) {
      errors.push({ text: "Please Write a Description" });
    }
    if (!img) {
        errors.push({ text: "Please Write a img" });
    }
    if (errors.length > 0) {
        res.render("news/new-news", {
          errors,
          title,
          description,
          img
        });
    }else{
        const newNew = new New({ title, description, img });
        await newNew.save();
        res.redirect('/news')
    }
     
});

router.get('/news', async (req, res) => {
    const news = await New.find().sort({date: 'desc'});
    res.render('news/all-news', { news })
});

router.get('/news/edit/:id', async (req,res) => {
    const ne = await New.findById(req.params.id);
    res.render('news/edit-news', { ne })
});

router.put('/news/edit-news/:id', async (req,res) => {
    const { title, description, img } = req.body;
    await New.findByIdAndUpdate(req.params.id, { title, description, img });
    res.redirect('/news');
});

router.delete('/news/delete/:id', async (req, res) => {
    await New.findByIdAndDelete(req.params.id);
    res.redirect('/news');
})

//extra comments
router.get('/news/view/:id', async (req,res) => {
    const eye = await New.findById(req.params.id);
    res.render('news/view-news', { eye })
})

router.put('/news/coment-news/:id', async (req, res) => {
    const {  coment } = req.body;
    await New.findByIdAndUpdate(req.params.id, { coment });
    
    console.log(coment);
    //res.send('okokok')    
    //res.redirect('/news/view/:id')
    res.redirect('/news/view/' + req.params.id);
})
module.exports= router;