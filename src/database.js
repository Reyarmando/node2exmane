const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/news-db-app', {
  useCreateIndex: true,
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true
})
  .then(db => console.log('DB is conecting'))
  .catch(err => console.error(err));