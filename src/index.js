const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');
const methodOverride = require('method-override');
const session = require('express-session');
const pug = require('pug');
// Iinitiali
const app = express();
require('./database');


// settings
app.use(express.static('public'));

app.set('port', process.env.PORT || 4000);

app.set('views', path.join(__dirname + '/views'));

app.set('view engine', 'pug');





// middlewares
app.use(express.urlencoded({extended: true}));
app.use(methodOverride('_method'));
app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}));

// routes
app.use(require('./routes/index'));
app.use(require('./routes/users'));
app.use(require('./routes/news'));


// static files
app.use(express.static(path.join(__dirname, 'public')));


// Server is listening
app.listen(app.get('port'), () => {
  console.log('Server on port', app.get('port'));
  
});